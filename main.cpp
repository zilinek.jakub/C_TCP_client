#include <iostream>
#include <iostream>
using namespace std;
#include <cstdlib>
#include <cstdio>
#include <sys/socket.h> // socket(), bind(), connect(), listen()
#include <unistd.h> // close(), read(), write()
#include <netinet/in.h> // struct sockaddr_in
#include <strings.h> // bzero()
#include <wait.h> // waitpid()
#include <cstring>
#include <utility>

#define BUFFER_SIZE 10240
#define TIMEOUT 1
#define DEBUG 0
#define TIMEOUT_RECHARGE 5
#define CLIENT_USERNAME 20
#define CLIENT_KEY_ID 5
#define CLIENT_CONFIRMATION 7
#define CLIENT_OK 12
#define CLIENT_RECHARGING "RECHARGING\a\b"
#define CLIENT_FULL_POWER "FULL POWER\a\b"
#define CLIENT_MESSAGE 100
#define SERVER_SYNTAX_ERROR "301 SYNTAX ERROR\a\b"
#define SERVER_KEY_REQUEST	"107 KEY REQUEST\a\b"
#define SERVER_LOGIN_FAILED "300 LOGIN FAILED\a\b"
#define SERVER_TURN_LEFT "103 TURN LEFT\a\b"
#define SERVER_KEY_OUT_OF_RANGE_ERROR "303 KEY OUT OF RANGE\a\b"
#define SERVER_OK "200 OK\a\b"
#define SERVER_MOVE	"102 MOVE\a\b"
#define SERVER_LOGOUT	"106 LOGOUT\a\b"
#define SERVER_TURN_RIGHT "104 TURN RIGHT\a\b"
#define SERVER_LOGIC_ERROR "302 LOGIC ERROR\a\b"
#define SERVER_PICK_UP	"105 GET MESSAGE\a\b"
#define MAX16 65536

enum Direction{m_left, m_down, m_right, m_up};

struct m_Position{
    int x;
    int y;
    Direction dir;
};

bool CheckSyntax(string cmd);

bool NavigateRobot(int & clientSocket, string & buffer);

bool GetRobotPosition(int & clientSocket, fd_set & sockets, string & buffer, m_Position & robPos);

bool MoveRobotToX0(int & clientSocket, string & buffer, m_Position & robPos, fd_set & sockets);

bool MoveRobot(int & clientSocket, fd_set & sockets, string & buffer, int & x ,int & y ,Direction & dir, string direction);

bool DodgeBarierX(int & clientSocket, fd_set & sockets, string & buffer, m_Position & robPos);

bool MoveRobotToY0(int & clientSocket,  string & buffer, m_Position & robPos, fd_set & sockets);

bool DodgeBarierY(int & clientSocket, fd_set & sockets, string & buffer, m_Position & robPos);

/** @returns true if in string had command in it*/
bool ParseString(string & in , string & command){
    string delimiter = "\a\b";
    size_t poss = in.find(delimiter);
    if ( poss != string::npos ){
        command = in.substr(0 , in.find(delimiter));
        command += "\a\b"; // OPTIONAL
        in.erase(0 , in.find(delimiter) + delimiter.length());
        return true;
    }
    return false;
}
bool InitServer(int argc, char **argv , int & serverSocket){
    if(argc < 2){
        cerr << "Usage: server port" << endl;
        return false;
    }
    serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    if(serverSocket < 0){
        perror("Nemohu vytvorit socket: ");
        return false;
    }
    int port = atoi(argv[1]);
    if(port == 0){
        cerr << "Usage: server port" << endl;
        close(serverSocket);
        return false;
    }

    struct sockaddr_in adresa;
    bzero(&adresa, sizeof(adresa));
    adresa.sin_family = AF_INET;
    adresa.sin_port = htons(port);
    adresa.sin_addr.s_addr = htonl(INADDR_ANY);

    // Prirazeni socketu k rozhranim
    if(bind(serverSocket, (struct sockaddr *) &adresa, sizeof(adresa)) < 0){
        perror("Problem s bind(): ");
        close(serverSocket);
        return false;
    }

    if(listen(serverSocket, 10) < 0){
        perror("Problem s listen()!");
        close(serverSocket);
        return false;
    }
    return true;
}
bool GetClient(int &clientSocket , int & serverSocket) {
    struct sockaddr_in vzdalena_adresa;
    socklen_t velikost;
    clientSocket = accept(serverSocket, (struct sockaddr *) &vzdalena_adresa, &velikost);
    if (clientSocket < 0) {
        perror("Problem s accept()!");
        close(serverSocket);
        return false;
    }
    return true;
}
bool SendMsg(int & clientSocket , const string msg){
    if (DEBUG)
        cout << "Server > " << msg << endl;
    if(send(clientSocket, msg.c_str(), msg.length() , MSG_NOSIGNAL) < 0){
        perror("Nemohu odeslat data:");
        return false;
    }
    return true;
}
bool ReadSocket(int & clientSocket , int & bytes , string & buffer){
    char bufferTmp[BUFFER_SIZE];
    bzero(&bufferTmp, sizeof(bufferTmp)); // OPTIONAL CLEARING OF BUFFER, MIGHT CAUSE PERFORMANCE ISSUES
    bytes = recv(clientSocket, bufferTmp, BUFFER_SIZE - 1, 0);
    if(bytes <= 0){
        perror("Chyba pri cteni ze socketu: ");
        return false;
    }
    if (bytes >= CLIENT_MESSAGE){
        SendMsg(clientSocket , string(SERVER_SYNTAX_ERROR));
        return false;
    }
    bufferTmp[bytes] = '\0';
    string response (bufferTmp , bytes);
    buffer += response;
    return true;
}
bool checkTimeOut(fd_set & sockets, timeval timeout, int &selRetVal , int & clientSocket) {
    FD_ZERO(&sockets);
    FD_SET(clientSocket, &sockets);
    selRetVal = select(clientSocket+1, &sockets , NULL , NULL , &timeout);
    if (selRetVal < 0){
        perror("Chyba v select(): ");
        return false;
    }
    if (!FD_ISSET(clientSocket , &sockets)){
        cout << "\tServer > Connection timeout on pid: " << getpid() << endl;
        return false;
    }
    return true;
}
bool GetMessage( fd_set & sockets , int & clientSocket , string & buffer  , string & clientMsg , int maxSize) {
    int selRetVal = 0;
    int bytes = 0;
    bool rechargeing = false;
    while(true) {
        if (ParseString(buffer, clientMsg)) {
            if (DEBUG)
                cout << "Client > " << clientMsg << endl;
            if (rechargeing){
                if (clientMsg != string(CLIENT_FULL_POWER)){
                    bytes -= string(CLIENT_FULL_POWER).size();
                    SendMsg(clientSocket , string(SERVER_LOGIC_ERROR));
                    return false;
                }
                else{
                    rechargeing = false;
                    continue;
                }
            }
            if ( clientMsg == string(CLIENT_RECHARGING) && !rechargeing){
                bytes -= string(CLIENT_RECHARGING).size();
                rechargeing = true;
                if (buffer.find("\a\b") != string::npos){
                    continue;
                }
            }
            if (clientMsg.size() > maxSize && !rechargeing) {
                SendMsg(clientSocket , string(SERVER_SYNTAX_ERROR));
                return false;
            }
            else if (!rechargeing)
                return true;
        }
        if ( bytes >= maxSize && !rechargeing) {
            SendMsg(clientSocket , string(SERVER_SYNTAX_ERROR));
            return false;
        }
        struct timeval timeout;
        if (rechargeing)
            timeout.tv_sec = TIMEOUT_RECHARGE;
        else
            timeout.tv_sec = TIMEOUT;
        timeout.tv_usec = 0;
        if (!checkTimeOut(sockets, timeout, selRetVal, clientSocket))
            return false;
        int bytesTmp;
        if (!ReadSocket(clientSocket, bytesTmp, buffer))
            return false;
        bytes += bytesTmp;
    }
}
int getKeyValue(int key) {
    if( key == 0 ) return 23019;
    if( key == 1 ) return 32037;
    if( key == 2 ) return 18789;
    if( key == 3 ) return 16443;
    if( key == 4 ) return 18189;
    return -1;
}
pair<int,int> countHash(int value, const string & name){
    int tmp;
    int res = 0;
    for(int i = 0 ; i < name.length() - 2 ; i++)
        res += name[i];
    res = (res * 1000) % MAX16;
    // RES -> ASCII name * 1000 mod 65356
    tmp = res;
    res = (res + value) % MAX16;
    return make_pair(res , tmp);
    // RES -> (ASCII name * 1000 + keyValue) mod 65356
    // TMP -> (ASCII name * 1000) mod 65356
}
int GetClientKeyID(int key) {
    if( key == 0) return 32037;
    if( key == 1) return 29295;
    if( key == 2) return 13603;
    if( key == 3) return 29533;
    if( key == 4) return 21952;
    return -1;
}
bool IsRightHash(int myHash, int clientHash, int clientKey) {
    if (clientKey <= -1)
        return false;
    myHash += clientKey;
    return clientHash == myHash % MAX16;
}
bool chechSpaces(string & toCheck) {
    for ( char & i : toCheck)
        if (i == ' ')
            return false;
    return true;
}
bool RobotLogin(int & serverSocket , int & clientSocket, string & buffer) {
    close(serverSocket);
    fd_set sockets;
    string name;

    if ( !GetMessage(sockets  , clientSocket ,buffer , name, CLIENT_USERNAME) )
        return false;
    if ( ! SendMsg(clientSocket , string(SERVER_KEY_REQUEST)) )
        return false;

    string keyStr;
    if ( !GetMessage(sockets  , clientSocket ,buffer , keyStr, CLIENT_KEY_ID) )
        return false;
    int key;
    if ( sscanf(keyStr.c_str(),"%d", &key ) != 1 ){
        SendMsg(clientSocket, string(SERVER_SYNTAX_ERROR));
        return false;
    }
    if (!chechSpaces(keyStr)){
        SendMsg(clientSocket, string(SERVER_SYNTAX_ERROR));
        return false;
    }
    int keyValue = getKeyValue(key);
    if (keyValue == -1) {
        SendMsg(clientSocket, string(SERVER_KEY_OUT_OF_RANGE_ERROR));
        return false;
    }

    pair<int , int> SendXRaw = countHash(keyValue , name);
    if ( ! SendMsg(clientSocket , to_string(SendXRaw.first) + "\a\b") )
        return false;
    string ClientHash;
    if ( !GetMessage(sockets  , clientSocket ,buffer , ClientHash, CLIENT_CONFIRMATION) )
        return false;

    uint32_t clientHash;
    if ( sscanf(ClientHash.c_str(), "%ul" , &clientHash) != 1 ){
        SendMsg(clientSocket, string(SERVER_KEY_OUT_OF_RANGE_ERROR));
        return false;
    }
    if (!chechSpaces(ClientHash)){
        SendMsg(clientSocket, string(SERVER_SYNTAX_ERROR));
        return false;
    }
    if (clientHash == -1) {
        SendMsg(clientSocket, string(SERVER_SYNTAX_ERROR));
        return false;
    }
    if (!IsRightHash(SendXRaw.second , clientHash , GetClientKeyID(key))) {
        SendMsg(clientSocket, string(SERVER_LOGIN_FAILED));
        return false;
    }
    if ( ! SendMsg(clientSocket, string(SERVER_OK)))
        return false;
    return true;
}

/*
 set follow-fork-mode child
 set detach-on-fork off
 */

int main(int argc, char **argv) {
    int serverSocket;
    if (!InitServer(argc, argv, serverSocket))
        return -1;
    while(true) {
        int clientSocket;
        if (!GetClient(clientSocket, serverSocket))
            return -1;
        pid_t pid = fork();
        if (pid == 0) {
            string buffer;
            if ( !RobotLogin(serverSocket, clientSocket, buffer) ) {
                close(clientSocket);
                return 0;
            }
            if ( !NavigateRobot(clientSocket, buffer) ) {
                close(clientSocket);
                return 0;
            }
            close(clientSocket);
            return 0;
        }
        int status = 0;
        waitpid(0, &status, WNOHANG);
        close(clientSocket);
    }
}

bool CheckSyntax(string cmd){
    if (cmd[0] != 'O') return false;
    if (cmd[1] != 'K') return false;
    if (cmd[2] != ' ') return false;
    bool min = false;
    bool white_space = false;
    for(int i = 3; i < cmd.size() - 2; i++){
        if (cmd[i] == '-' && min)
            return false;
        else if (cmd[i] == ' ' && white_space)
            return false;
        else if (cmd[i] == ' ')
            white_space = true;
        else if (cmd[i] == '-')
            min = true;
        else if (cmd[i] < '0' || cmd[i] > '9') {
            return false;
        }
        else
            min = false;
    }
    return true;
}

bool NavigateRobot(int & clientSocket, string & buffer) {
    fd_set sockets;
    m_Position robPos{};
    if ( ! GetRobotPosition(clientSocket , sockets , buffer , robPos) )
        return false;
    if ( ! MoveRobotToX0(clientSocket, buffer , robPos, sockets) )
        return false;
    if ( ! MoveRobotToY0(clientSocket , buffer , robPos , sockets) )
        return false;
    if ( ! SendMsg(clientSocket , SERVER_PICK_UP) )
        return false;
    string result;
    if ( !GetMessage(sockets, clientSocket , buffer , result, CLIENT_MESSAGE))
        return false;
    if ( ! SendMsg(clientSocket , SERVER_LOGOUT) )
        return false;
    return true;
}

bool MoveRobotToY0(int & clientSocket, string & buffer, m_Position & robPos, fd_set & sockets) {
    while(robPos.y != 0) {
        if (robPos.y < 0 && robPos.dir != m_up){
            if (!MoveRobot(clientSocket, sockets, buffer, robPos.x, robPos.y,robPos.dir, SERVER_TURN_LEFT))
                return false;
        }
        else if (robPos.y > 0 && robPos.dir != m_down){
            if (!MoveRobot(clientSocket, sockets, buffer, robPos.x, robPos.y,robPos.dir, SERVER_TURN_LEFT))
                return false;
        }
        else{
            int x = robPos.x, y = robPos.y;
            if (!MoveRobot(clientSocket, sockets, buffer, robPos.x, robPos.y,robPos.dir, SERVER_MOVE))
                return false;
            if (robPos.x == x && robPos.y == y){
                if ( !DodgeBarierY(clientSocket, sockets, buffer, robPos) )
                    return false;
            }
        }
    }

    return true;
}

bool DodgeBarierY(int & clientSocket, fd_set & sockets, string & buffer, m_Position & robPos) {
    if (! MoveRobot(clientSocket , sockets, buffer , robPos.x , robPos.y, robPos.dir , SERVER_TURN_LEFT))
        return false;
    if (! MoveRobot(clientSocket , sockets, buffer , robPos.x , robPos.y, robPos.dir , SERVER_MOVE))
        return false;
    if (! MoveRobot(clientSocket , sockets, buffer , robPos.x , robPos.y, robPos.dir , SERVER_TURN_RIGHT))
        return false;
    if (! MoveRobot(clientSocket , sockets, buffer , robPos.x , robPos.y, robPos.dir , SERVER_MOVE))
        return false;
    if (! MoveRobot(clientSocket , sockets, buffer , robPos.x , robPos.y, robPos.dir , SERVER_MOVE))
        return false;
    if (! MoveRobot(clientSocket , sockets, buffer , robPos.x , robPos.y, robPos.dir , SERVER_TURN_RIGHT))
        return false;
    if (! MoveRobot(clientSocket , sockets, buffer , robPos.x , robPos.y, robPos.dir , SERVER_MOVE))
        return false;
    if (! MoveRobot(clientSocket , sockets, buffer , robPos.x , robPos.y, robPos.dir , SERVER_TURN_LEFT))
        return false;
    return true;
}

bool MoveRobotToX0(int & clientSocket, string & buffer, m_Position & robPos , fd_set & sockets) {
    while(robPos.x != 0){
        if (robPos.x < 0 && robPos.dir != m_right) {
            if (!MoveRobot(clientSocket, sockets, buffer, robPos.x, robPos.y,robPos.dir, SERVER_TURN_LEFT))
                return false;
        }
        else if (robPos.x > 0 && robPos.dir != m_left) {
            if (!MoveRobot(clientSocket, sockets, buffer, robPos.x, robPos.y,robPos.dir, SERVER_TURN_LEFT))
                return false;
        }
        else {
            int x = robPos.x, y = robPos.y;
            if (!MoveRobot(clientSocket, sockets, buffer, robPos.x, robPos.y,robPos.dir, SERVER_MOVE))
                return false;
            if (robPos.x == x && robPos.y == y){
                if ( !DodgeBarierX(clientSocket, sockets, buffer, robPos) )
                    return false;
            }
        }
    }
    return true;
}

bool DodgeBarierX(int & clientSocket, fd_set &sockets, string & buffer, m_Position & robPos) {
    if (robPos.y < 0 && robPos.x < 0 || robPos.y > 0 && robPos.x > 0){
        if (! MoveRobot(clientSocket , sockets, buffer , robPos.x , robPos.y, robPos.dir , SERVER_TURN_LEFT))
            return false;
        if (! MoveRobot(clientSocket , sockets, buffer , robPos.x , robPos.y, robPos.dir , SERVER_MOVE))
            return false;
        if (! MoveRobot(clientSocket , sockets, buffer , robPos.x , robPos.y, robPos.dir , SERVER_TURN_RIGHT))
            return false;
        if (! MoveRobot(clientSocket , sockets, buffer , robPos.x , robPos.y, robPos.dir , SERVER_MOVE))
            return false;
        return true;
    }
    else {
        if (! MoveRobot(clientSocket , sockets, buffer , robPos.x , robPos.y, robPos.dir , SERVER_TURN_RIGHT))
            return false;
        if (! MoveRobot(clientSocket , sockets, buffer , robPos.x , robPos.y, robPos.dir , SERVER_MOVE))
            return false;
        if (! MoveRobot(clientSocket , sockets, buffer , robPos.x , robPos.y, robPos.dir , SERVER_TURN_LEFT))
            return false;
        if (! MoveRobot(clientSocket , sockets, buffer , robPos.x , robPos.y, robPos.dir , SERVER_MOVE))
            return false;
        return true;
    }
}

bool MoveRobot(int & clientSocket, fd_set & sockets, string & buffer, int & x ,int & y , Direction & dir, string direction){
    string bufferTMP;
    if ( !SendMsg(clientSocket, direction ) )
        return false;
    if (direction == SERVER_TURN_LEFT){
        if (dir == m_left)
            dir = m_down;
        else if (dir == m_down)
            dir = m_right;
        else if (dir == m_right)
            dir = m_up;
        else
            dir = m_left;
    }
    if (direction == SERVER_TURN_RIGHT){
        if (dir == m_left)
            dir = m_up;
        else if (dir == m_up)
            dir = m_right;
        else if (dir == m_right)
            dir = m_down;
        else
            dir = m_left;
    }

    if ( !GetMessage(sockets, clientSocket , buffer , bufferTMP, CLIENT_OK + 1))
        return false;
    if ( !CheckSyntax(bufferTMP)) {
        SendMsg(clientSocket, SERVER_SYNTAX_ERROR);
        return false;
    }
    if ( sscanf(bufferTMP.c_str(),"OK %d %d",&x,&y) != 2 ) {
        return false;
    }
    return true;
}

bool GetRobotPosition(int & clientSocket, fd_set & sockets, string & buffer, m_Position & robPos) {
    int x, y;
    int After_x, After_y;
    Direction dir;
    if (!MoveRobot(clientSocket, sockets, buffer, x, y,dir, SERVER_MOVE))
        return false;
    if (x == 0 && y == 0){
        robPos.x = 0;
        robPos.y = 0;
        return true;
    }
    while(true){
        if (!MoveRobot(clientSocket, sockets, buffer, After_x, After_y, dir , SERVER_MOVE))
            return false;
        if (After_y == 0 && After_x == 0){
            robPos.x = 0;
            robPos.y = 0;
            return true;
        }
        if (x != After_x || y != After_y) {
            break;
        }
        if (!MoveRobot(clientSocket, sockets, buffer, After_x, After_y, dir , SERVER_TURN_LEFT))
            return false;
    }
    robPos.x = After_x;
    robPos.y = After_y;

    if (x < After_x)
        robPos.dir = m_right;
    else if (y < After_y)
        robPos.dir = m_up;
    else if (y > After_y)
        robPos.dir = m_down;
    else if (x < After_x)
        robPos.dir = m_left;
    return true;
}

